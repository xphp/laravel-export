<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Export
Route::get('/siswa' , 'SiswaController@index');
Route::get('/siswa/export_excel' , 'SiswaController@export_excel');

// Import
Route::post('/siswa/import_excel' , 'SiswaController@import_excel');

// multi bahasa / Localization
Route::get('/form' , function() {
  return view('biodata');
});

// localization pilih bahasa
Route::get('/{locale}/form', function ($locale) {
  App::setLocale($locale);

  return view('biodata');
})->name('locale');

// Action URL
Route::get('/halo/{nama}' , 'HaloController@halo');
Route::get('/halo' , 'HaloController@panggil');


