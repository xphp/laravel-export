<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;                          // Model Siswa

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SiswaExport;            // Export
use App\Imports\SiswaImport;            // Import
use Session;                            // Session

class SiswaController extends Controller
{
  public function index()
  {
    $siswa = Siswa::all();
    return view('siswa', compact('siswa'));
  }

  //* Export
  public function export_excel()
  {
    return Excel::download(new SiswaExport, 'siswa.xlsx');
  }

  //* Import
  public function import_excel(Request $request)
  {
    // validasi
    $this->validate($request, [
      // 'file' => 'required|mimes:csv,xls,xlsx'
      'file' => 'required'
    ]);

    // menangkap file excel
    $file = $request->file('file');

    // membuat nama file unik
    $nama_file = rand().$file->getClientOriginalName();

    // upload ke folder file_siswa di dalam folder public
    $file->move('file_siswa', $nama_file);

    // import data
    Excel::import(new SiswaImport, public_path('/file_siswa/'.$nama_file));

    // notifikasi dengan session
    Session::flash('sukses', 'Data Siswa Berhasil Diimport!');

    // alihkan halaman kembali
    return redirect('/siswa');
  }
}
