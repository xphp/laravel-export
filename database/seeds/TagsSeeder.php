<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create('id_ID');

      for ($i=1; $i < 11; $i++) {
        DB::table('tags')->insert([
          'tag' => $faker->name,
          'article_id' => $i,
        ]);
      }
    }
}
