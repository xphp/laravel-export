<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PegawaisSedeer extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // data faker indo
    $faker = Faker::create('id_ID');

    // membuat data dummy
    for ($i=1; $i < 11; $i++) {
      DB::table('pegawais')->insert([
        'nama'    => $faker->name,
        'alamat'  => $faker->address,
      ]);
    }
  }
}
