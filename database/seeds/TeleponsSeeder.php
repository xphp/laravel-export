<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TeleponsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create('id_ID');

      for ($i=1; $i < 11; $i++) {

        DB::table('telepons')->insert([
          'nomor_telepon' => $faker->phoneNumber
        ]);
      }
    }
}
