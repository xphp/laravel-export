<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <title>Tutorial Multi Bahasa Localization Laravel</title>
</head>

<body>

  <center>
    <h2>@lang('biodata.welcome')</h2>
    <h4><a href="#">www.malasngoding.com</a></h4>
  </center>

  <div class="conteiner text-left">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">@lang('biodata.title')</h5>
          </div>
          <div class="card-body">
            <form action="#">
              <div class="form-group">
                <label for="name">@lang('biodata.profil.name')</label>
                <input id="name" class="form-control" type="text" name="">
              </div>
              <div class="form-group">
                <label for="address">@lang('biodata.profil.address')</label>
                <input id="address" class="form-control" type="text" address="">
              </div>
              <div class="form-group">
                <label for="hobby">@lang('biodata.profil.hobby')</label>
                <input id="hobby" class="form-control" type="text" hobby="">
              </div>
              <div class="form-group">
                <label for="job">@lang('biodata.profil.job')</label>
                <input id="job" class="form-control" type="text" job="">
              </div>
              <div class="form-group">
                <button class="btn btn-primary">@lang('biodata.button')</button>
              </div>
            </form>
          </div>
          <div class="card-footer">
            <p>@lang('biodata.thank')</p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
  </script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
  </script>
</body>

</html>
